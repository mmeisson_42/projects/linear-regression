extern crate csv;
#[macro_use]
extern crate serde_derive;

use std::fs::File;
use std::error::Error;
use std::process::exit;


#[derive(Debug, Deserialize)]
struct Record {
	km: u32,
	price: u32,
}

#[derive(Debug, Serialize)]
struct LineDefinition {
	slope: f32,
	origin: f32,
}


fn get_records(file_name: &String) -> Result<Vec<Record>, Box<Error>> {
	let file = File::open(file_name)?;
	let mut reader = csv::Reader::from_reader(file);
	let mut records = Vec::new();

	for result in reader.deserialize() {
		records.push(result?);
	}
	Ok(records)
}

fn put_gradient(file_name: &String, line: &LineDefinition) -> Result<(), Box<Error>> {
	let file = File::create(file_name)?;
	let mut writer = csv::Writer::from_writer(file);

	writer.serialize(line)?;
	writer.flush()?;
	
	Ok(())
}

fn gradient_descent(line: &mut LineDefinition, dataset: &Vec<Record>) {
	let learning_rate = 0.5 * (1.0 / dataset.len() as f32);

	for record in dataset {
		let x = record.km as f32 / 500000.0;
		let y = record.price as f32 / 10000.0;

		let expected = line.origin + line.slope * x;
		let error = y - expected;

		line.slope += error * x * learning_rate;
		line.origin += error * learning_rate;
	}
}


fn main() {
	let mut line = LineDefinition { slope: -1.0, origin: 1.0 };

	let dataset = match get_records(&String::from("data.csv")) {
		Ok(data) => data,
		Err(e) => {
			println!("No dataset found: {:?}", e);
			exit(1)
		},
	};

	gradient_descent(&mut line, &dataset);

	match put_gradient(&String::from("../calculus.csv"), &line) {
		Ok(_) => (),
		Err(err) => println!("{:?}", err),
	};
}
